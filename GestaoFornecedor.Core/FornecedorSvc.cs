﻿using GestaoFornecedor.Domain;
using GestaoFornecedor.Infra;
using System.Collections.Generic;

namespace GestaoFornecedor.Core
{
    public class FornecedorSvc
    {
        FornecedorRepo _repositorio;
        public FornecedorRepo Repositorio
        {
            get
            {
                return _repositorio ?? (_repositorio = new FornecedorRepo());
            }
        }

        public EstruturaRetorno AdicionarFornecedor(Fornecedor fornecedor)
        {
            EstruturaRetorno estrutura = new EstruturaRetorno();

            if (!Repositorio.VerificarFornecedorExistente(fornecedor.Nome)
                && fornecedor.DataFinalContrato > fornecedor.DataInicioContrato)
            {
                Repositorio.Create(fornecedor);

                estrutura.Mensagem = "Fornecedor salvo com sucesso";
            }
            else
            {
                estrutura.PossuiErro = true;
                estrutura.Mensagem = "Verificar se o nome do fornecedor já existe e se os campos de data estão corretos";
            }

            return estrutura;
        }

        public List<Fornecedor> ListaFornecedores()
        {
            return Repositorio.List();
        }

        public Fornecedor GetFornecedorPorId(int id)
        {
            return Repositorio.GetFornecedorPorId(id);
        }

        public Fornecedor GetFornecedorPorNome(string nome)
        {
            return Repositorio.GetFornecedorPorNome(nome);
        }

        public EstruturaRetorno Editar(Fornecedor fornecedor)
        {
            EstruturaRetorno estrutura = new EstruturaRetorno();

            if (fornecedor.DataFinalContrato > fornecedor.DataInicioContrato)
            {
                Repositorio.Editar(fornecedor);
                estrutura.Mensagem = "Fornecedor salvo com sucesso";
            }
            else
            {
                estrutura.PossuiErro = true;
                estrutura.Mensagem = "Data inicial maior que a data final";
            }

            return estrutura;
        }

        public void Delete(int id)
        {
            Repositorio.Delete(id);
        }
    }
}