﻿using GestaoFornecedor.Domain;
using GestaoFornecedor.Infra;
using System.Collections.Generic;

namespace GestaoFornecedor.Core
{
    public class ContatoSvc
    {
        ContatoRepo _repositorio;
        public ContatoRepo Repositorio
        {
            get
            {
                return _repositorio ?? (_repositorio = new ContatoRepo());
            }
        }

        public void AdicionarContato(Contato contato)
        {
            if (contato.Telefone != null || contato.Email != null)
            {
                if (contato.Telefone != null)
                {
                    contato.Telefone = contato.Telefone.Replace("-", string.Empty);
                }

                Repositorio.Create(contato);
            }
        }

        public List<Contato> ListaContatoPorFornecedor(int id)
        {
            return Repositorio.ListaContatoPorFornecedor(id);
        }

        public Contato GetContatoPorId(int id)
        {
            return Repositorio.GetContatoPorId(id);
        }

        public void Editar(Contato contato)
        {
            if (contato.Telefone != null)
            {
                contato.Telefone = contato.Telefone.Replace("-", string.Empty);
            }
            
            Repositorio.Editar(contato);
        }

        public void Delete(int id)
        {
            Repositorio.Delete(id);
        }
    }
}