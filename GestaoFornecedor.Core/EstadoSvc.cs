﻿using GestaoFornecedor.Domain;
using GestaoFornecedor.Infra;
using System.Collections.Generic;

namespace GestaoFornecedor.Core
{
    public class EstadoSvc
    {
        EstadoRepo _repositorio;
        public EstadoRepo Repositorio
        {
            get
            {
                return _repositorio ?? (_repositorio = new EstadoRepo());
            }
        }

        public List<Estado> ListaEstados()
        {
            return Repositorio.List();
        }
    }
}