﻿using GestaoFornecedor.Domain;
using GestaoFornecedor.Infra;
using System.Collections.Generic;

namespace GestaoFornecedor.Core
{
    public class CidadeSvc
    {
        CidadeRepo _repositorio;
        public CidadeRepo Repositorio
        {
            get
            {
                return _repositorio ?? (_repositorio = new CidadeRepo());
            }
        }

        public List<Cidade> ListaCidadePorEstado(int id)
        {
            return Repositorio.ListaCidadePorEstado(id);
        }

        public Cidade GetCidadePorId(int id)
        {
            return Repositorio.GetCidadePorId(id);
        }
    }
}