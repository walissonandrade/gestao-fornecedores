﻿using GestaoFornecedor.Domain;
using System.Data.Entity.ModelConfiguration;

namespace GestaoFornecedor.Infra
{
    public class EstadoMap : EntityTypeConfiguration<Estado>
    {
        public EstadoMap()
        {
            ToTable("Estado");
        }
    }
}
