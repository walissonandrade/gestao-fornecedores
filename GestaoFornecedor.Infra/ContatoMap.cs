﻿using GestaoFornecedor.Domain;
using System.Data.Entity.ModelConfiguration;

namespace GestaoFornecedor.Infra
{
    public class ContatoMap : EntityTypeConfiguration<Contato>
    {
        public ContatoMap()
        {
            ToTable("Contato");
        }
    }
}
