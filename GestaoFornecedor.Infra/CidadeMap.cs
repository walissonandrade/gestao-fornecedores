﻿using GestaoFornecedor.Domain;
using System.Data.Entity.ModelConfiguration;

namespace GestaoFornecedor.Infra
{
    public class CidadeMap : EntityTypeConfiguration<Cidade>
    {
        public CidadeMap()
        {
            ToTable("Cidade");
        }
    }
}