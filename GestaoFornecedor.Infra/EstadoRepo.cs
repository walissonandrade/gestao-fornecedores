﻿using GestaoFornecedor.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestaoFornecedor.Infra
{
    public class EstadoRepo
    {
        private Contexto _contexto { get; set; }
        public EstadoRepo()
        {
            _contexto = new Contexto();
        }

        public List<Estado> List()
        {
            return _contexto.Estado.ToList();
        }

    }
}
