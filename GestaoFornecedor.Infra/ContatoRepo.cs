﻿using GestaoFornecedor.Domain;
using System.Collections.Generic;
using System.Linq;


namespace GestaoFornecedor.Infra
{
    public class ContatoRepo
    {
        private Contexto _contexto { get; set; }
        public ContatoRepo()
        {
            _contexto = new Contexto();
        }

        public void Create(Contato contato)
        {
            _contexto.Contatos.Add(contato);
            _contexto.SaveChanges();
        }

        public List<Contato> ListaContatoPorFornecedor(int id)
        {
            return _contexto.Contatos.Where(c => c.FornecedorId == id).ToList();
        }

        public Contato GetContatoPorId(int id)
        {
            return _contexto.Contatos.Where(c => c.Id == id).First();
        }

        public void Editar(Contato contato)
        {
            _contexto.Entry<Contato>(contato).State = System.Data.Entity.EntityState.Modified;
            _contexto.SaveChanges();
        }

        public void Delete(int id)
        {
            Contato contato = _contexto.Contatos.Where(f => f.Id == id).FirstOrDefault();
            _contexto.Contatos.Remove(contato);
            _contexto.SaveChanges();
        }
    }
}
