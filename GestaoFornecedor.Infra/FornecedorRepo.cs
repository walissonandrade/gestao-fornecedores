﻿using GestaoFornecedor.Domain;
using System.Collections.Generic;
using System.Linq;

namespace GestaoFornecedor.Infra
{
    public class FornecedorRepo
    {

        private Contexto _contexto { get; set; }
        public FornecedorRepo()
        {
            _contexto = new Contexto();
        }

        /// <summary>
        /// Metodo para gravar fornecedor no banco de dados
        /// </summary>
        /// <param name="fornecedor"></param>
        public void Create(Fornecedor fornecedor)
        {
            _contexto.Fornecedores.Add(fornecedor);
            _contexto.SaveChanges();
        }

        public List<Fornecedor> List()
        {
            var query = from f in _contexto.Fornecedores.ToList()
                        join c in _contexto.Cidade.ToList() on f.CidadeId equals c.Id
                        join e in _contexto.Estado.ToList() on c.EstadoId equals e.Id
                        select f;

            var result = query.AsEnumerable().ToList();
            return result;

            //return _contexto.Fornecedores.Include("Cidade").ToList();
        }

        public Fornecedor GetFornecedorPorId(int id)
        {
            var query = from f in _contexto.Fornecedores.Include("Contato").ToList()
                        join c in _contexto.Cidade.ToList() on f.CidadeId equals c.Id
                        join e in _contexto.Estado.ToList() on c.EstadoId equals e.Id
                        where f.Id == id
                        select f;

            var result = query.First();
            return result;

            //return _contexto.Fornecedores.Include("Contato").Include("Cidade").Where(f => f.Id == id).FirstOrDefault();
            //return _contexto.Fornecedores.Where(f => f.Id == id).FirstOrDefault();
        }

        public Fornecedor GetFornecedorPorNome(string Nome)
        {
            var query = from f in _contexto.Fornecedores.Include("Contato").ToList()
                        join c in _contexto.Cidade.ToList() on f.CidadeId equals c.Id
                        join e in _contexto.Estado.ToList() on c.EstadoId equals e.Id
                        where f.Nome.ToLower() == Nome.ToLower()
                        select f;

            var result = query.First();
            return result;
        }

        public void Editar(Fornecedor fornecedor)
        {
            _contexto.Entry<Fornecedor>(fornecedor).State = System.Data.Entity.EntityState.Modified;
            _contexto.SaveChanges();
            
        }

        public void Delete(int id)
        {
            Fornecedor fornecedor = _contexto.Fornecedores.Where(f => f.Id == id).FirstOrDefault();
            _contexto.Fornecedores.Remove(fornecedor);
            _contexto.SaveChanges();
        }

        public bool VerificarFornecedorExistente(string nome)
        {
            return _contexto.Fornecedores.Any(a => a.Nome.ToLower() == nome.ToLower());
        }
    }
}