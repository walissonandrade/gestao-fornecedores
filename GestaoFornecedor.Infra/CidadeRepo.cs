﻿using GestaoFornecedor.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestaoFornecedor.Infra
{
    public class CidadeRepo
    {
        private Contexto _contexto { get; set; }
        public CidadeRepo()
        {
            _contexto = new Contexto();
        }

        public List<Cidade> ListaCidadePorEstado(int id)
        {
            return _contexto.Cidade.Where(c => c.EstadoId == id).ToList();
        }

        public Cidade GetCidadePorId(int id)
        {
            return _contexto.Cidade.Where(c => c.Id == id).FirstOrDefault();
        }
    }
}
