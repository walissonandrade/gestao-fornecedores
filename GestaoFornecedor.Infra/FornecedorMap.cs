﻿using GestaoFornecedor.Domain;
using System.Data.Entity.ModelConfiguration;

namespace GestaoFornecedor.Infra
{
    public class FornecedorMap : EntityTypeConfiguration<Fornecedor>
    {
        public FornecedorMap()
        {
            ToTable("Fornecedor");
        }
    }
}