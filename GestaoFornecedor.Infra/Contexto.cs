﻿using GestaoFornecedor.Domain;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;

namespace GestaoFornecedor.Infra
{
    public class Contexto : DbContext 
    {
        public Contexto() : base("FornecedorConnectionString")
        {
            this.Configuration.LazyLoadingEnabled = false;
        }

        public DbSet <Fornecedor> Fornecedores { get; set; }
        public DbSet <Contato> Contatos { get; set; }
        public DbSet <Estado> Estado { get; set; }
        public DbSet <Cidade> Cidade { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            // FORNECEDOR
            // Chave primária para tabela fornecedor
            modelBuilder.Entity<Fornecedor>().HasKey(f => f.Id);
            // Chave identity key para fornecedor id
            modelBuilder.Entity<Fornecedor>().Property(f => f.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            modelBuilder.Entity<Fornecedor>().Property(f => f.Nome).HasMaxLength(150);
            // A chave estrangeira para a tabela Fornecedor - EstadoId
        //     modelBuilder.Entity<Fornecedor>().HasRequired(e => e.Estado)
        //     .WithMany(f => f.Fornecedor).HasForeignKey(f => f.EstadoId);
            // A chave estrangeira para a tabela Fornecedor - CidadeId
            modelBuilder.Entity<Fornecedor>().HasRequired(c => c.Cidade)
             .WithMany(f => f.Fornecedor).HasForeignKey(f => f.CidadeId);

            // CONTATO
            // chave primária para tabela contato
            modelBuilder.Entity<Contato>().HasKey(c => c.Id);
            // chave  identity para contato id
            modelBuilder.Entity<Contato>().Property(c => c.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            modelBuilder.Entity<Contato>().Property(c => c.Nome).HasMaxLength(150);
            modelBuilder.Entity<Contato>().Property(c => c.Email).HasMaxLength(150);
            modelBuilder.Entity<Contato>().Property(c => c.Telefone).HasMaxLength(10);

            // A chave estrangeira para a tabela Contato - FornecedorId
            modelBuilder.Entity<Contato>().HasRequired(f => f.Fornecedor)
             .WithMany(c => c.Contato).HasForeignKey(c => c.FornecedorId);

            // ESTADO
            // Chave primária para tabela estado
            modelBuilder.Entity<Estado>().HasKey(e => e.Id);
            // chave identity para contato id
            modelBuilder.Entity<Estado>().Property(e => e.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            // CIDADE
            // Chave primaria para tabela cidade
            modelBuilder.Entity<Cidade>().HasKey(c => c.Id);
            // Chave identity para contato id
            modelBuilder.Entity<Cidade>().Property(c => c.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            // Chave estrangeira para tabela cidade -EstadoId
            modelBuilder.Entity<Cidade>().HasRequired(c => c.Estado)
             .WithMany(e => e.Cidade).HasForeignKey(e => e.EstadoId);

            // Deletar em cascata a partir de Fornecedor para Contato
            modelBuilder.Entity<Contato>()
                .HasRequired(f => f.Fornecedor)
                .WithMany(c => c.Contato)
                .HasForeignKey(c => c.FornecedorId)
                .WillCascadeOnDelete(true);
            base.OnModelCreating(modelBuilder);

            // Deletar em cascata a partir de Cidade para Fornecedor
            modelBuilder.Entity<Fornecedor>()
                .HasRequired(c => c.Cidade)
                .WithMany(f => f.Fornecedor)
                .HasForeignKey(f => f.CidadeId)
                .WillCascadeOnDelete(true);
            base.OnModelCreating(modelBuilder);

            // Deletar em cascata a partir de Estado para Fornecedor
            //modelBuilder.Entity<Fornecedor>()
            //    .HasRequired(e => e.Estado)
            //    .WithMany(f => f.Fornecedor)
            //    .HasForeignKey(f => f.EstadoId)
            //    .WillCascadeOnDelete(true);
            //base.OnModelCreating(modelBuilder);

            // Deletar em cascata a partir de Estado para Cidade
            modelBuilder.Entity<Cidade>()
                .HasRequired(e => e.Estado)
                .WithMany(c => c.Cidade)
                .HasForeignKey(c => c.EstadoId)
                .WillCascadeOnDelete(true);
            base.OnModelCreating(modelBuilder);

           modelBuilder.Configurations.Add(new FornecedorMap());
            modelBuilder.Configurations.Add(new ContatoMap());
            modelBuilder.Configurations.Add(new EstadoMap());
            modelBuilder.Configurations.Add(new CidadeMap());
        }       
    }
}