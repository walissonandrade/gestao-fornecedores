﻿using System.Collections.Generic;

namespace GestaoFornecedor.Domain
{
    public class Cidade
    {
        public Cidade()
        {
            Fornecedor = new List<Fornecedor>();
        }

        public int Id { get; set; }
        public string Nome { get; set; }

        public int EstadoId { get; set; }
        public virtual Estado Estado { get; set; }

        public virtual ICollection<Fornecedor> Fornecedor { get; set; }
    }
}