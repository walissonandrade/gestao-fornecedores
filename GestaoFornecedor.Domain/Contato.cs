﻿using System.ComponentModel.DataAnnotations;

namespace GestaoFornecedor.Domain
{
    public class Contato
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Nome deve ser preenchido")]
        [MaxLength(150, ErrorMessage ="Tamanho máximo do nome é 150")]
        public string Nome { get; set; }
        [MaxLength(150, ErrorMessage = "Tamanho máximo do email é 150")]
        public string Email { get; set; }
        [MaxLength(10, ErrorMessage = "Tamanho máximo do telefone é 10")]
        public string Telefone { get; set; }
        public int FornecedorId { get; set; }

        public virtual Fornecedor Fornecedor { get; set; }

        // OBS é necessário preenchimento do telefone ou e-mail
    }
}