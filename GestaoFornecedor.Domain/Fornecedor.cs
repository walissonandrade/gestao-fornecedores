﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace GestaoFornecedor.Domain
{
    public class Fornecedor
    {
        public Fornecedor()
        {
            Contato = new List<Contato>();
        }

        public int Id { get; set; }

        [Required(ErrorMessage = "Nome deve ser preenchido")]
        [MaxLength(150, ErrorMessage = "O nome deve ter no máximo 150 caracteres")]
        public string Nome { get; set; }

        public bool Status { get; set; }

        [Required(ErrorMessage = "Data início contrato deve ser preenchida")]
        public DateTime DataInicioContrato { get; set; }

        [Required(ErrorMessage = "Data final contrato deve ser preenchida")]
        public DateTime DataFinalContrato { get; set; }

        public float ValorMaximoPorDemanda { get; set; }

        //public int EstadoId { get; set; }
        //public virtual Estado Estado { get; set; }

        public int CidadeId { get; set; }
        public virtual Cidade Cidade { get; set; }

        public virtual ICollection<Contato> Contato { get; set; }
    }
}