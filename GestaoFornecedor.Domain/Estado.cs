﻿using System.Collections.Generic;

namespace GestaoFornecedor.Domain
{
    public class Estado
    {
        public int Id { get; set; }
        public string Nome { get; set; }

     //   public virtual ICollection<Fornecedor> Fornecedor { get; set; }
        public virtual ICollection<Cidade> Cidade { get; set; }
    }
}