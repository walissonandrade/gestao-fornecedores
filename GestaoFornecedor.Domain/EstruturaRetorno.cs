﻿namespace GestaoFornecedor.Domain
{
    public class EstruturaRetorno
    {
        public bool PossuiErro { get; set; }

        public string DescricaoErro { get; set; }

        public string Mensagem { get; set; }
    }
}