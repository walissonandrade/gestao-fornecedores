﻿using GestaoFornecedor.Core;
using GestaoFornecedor.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace GestaoFornecedor.Web.Controllers
{
    public class FornecedorController : Controller
    {
        FornecedorSvc _fornecedorSvc;
        FornecedorSvc FornecedorSvc
        {
            get
            {
                return _fornecedorSvc ?? (_fornecedorSvc = new FornecedorSvc());
            }
        }

        EstadoSvc _estadoSvc;
        EstadoSvc EstadoSvc
        {
            get
            {
                return _estadoSvc ?? (_estadoSvc = new EstadoSvc());
            }
        }

        // GET: Fornecedor
        public ActionResult Index()
        {
            List<Fornecedor> fornecedores = FornecedorSvc.ListaFornecedores();

            return View(fornecedores);
        }

        public ActionResult Cadastrar()
        {
            // Buscando Estados
            var listaEstados = EstadoSvc.ListaEstados();

            listaEstados.Insert(0, new Estado());
            ViewBag.Estado = new SelectList(listaEstados, "Id", "Nome");
            ViewBag.Sucesso = null;

            return View();
        }

        [HttpPost]
        public ActionResult Cadastrar(Fornecedor fornecedor, string NomeContato, string Telefone, string Email)
        {
            // Buscando Estados
            var listaEstados = EstadoSvc.ListaEstados();
            ViewBag.Estado = new SelectList(listaEstados, "Id", "Nome");

            if (ModelState.IsValid)
            {
                var estruturaRetorno = FornecedorSvc.AdicionarFornecedor(fornecedor);

                if (estruturaRetorno.PossuiErro)
                    ViewBag.Erro = estruturaRetorno.Mensagem;
                else
                {
                    if (NomeContato != null &&
                        (Telefone != null || Email != null))
                    {
                        var serviceContato = new ContatoSvc();
                        Contato contato = new Contato();
                        contato.Nome = NomeContato;
                        contato.Telefone = Telefone;
                        contato.Email = Email;
                        contato.FornecedorId = FornecedorSvc.GetFornecedorPorNome(fornecedor.Nome).Id;
                        serviceContato.AdicionarContato(contato);
                        ViewBag.Sucesso = estruturaRetorno.Mensagem;
                    }
                    else
                        ViewBag.Sucesso = "Fornecedor Cadastrado Com sucesso. Contato não foi cadastrado por inconsistência nos dados";
                }                    

                return View();
            }
            
            return View("Cadastrar", fornecedor);
        }

        public ActionResult ListaCidade(string id)
        {
            var serviceCidade = new CidadeSvc();
            List<Cidade> cidades = serviceCidade.ListaCidadePorEstado(Convert.ToInt32(id));

            return Json(cidades.Select(s => new
            {
                s.Id,
                s.Nome
            }), JsonRequestBehavior.AllowGet);
        }

        public ActionResult Editar(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            ViewBag.Sucesso = null;
            ViewBag.Contato = new Contato();

            var fornecedor = FornecedorSvc.GetFornecedorPorId(Convert.ToInt32(id));

            // Buscando Estados
            var listaEstados = EstadoSvc.ListaEstados();

            //listaEstados.Insert(0, new Estado());
            ViewBag.Estado = new SelectList(listaEstados, "Id", "Nome", fornecedor.Cidade.Estado.Nome);

            return View(fornecedor);
        }

        [HttpPost]
        public ActionResult Editar(Fornecedor fornecedor)
        {
            ViewBag.Contato = new Contato();

            if (ModelState.IsValid)
            {
                var estruturaRetorno = FornecedorSvc.Editar(fornecedor);
                fornecedor = FornecedorSvc.GetFornecedorPorId(fornecedor.Id);

                // Buscando Estados
                var SvcEstado = new EstadoSvc();

                ViewBag.Estado = new SelectList(EstadoSvc.ListaEstados(), "Id", "Nome", fornecedor.Cidade.Estado.Nome);

                if (estruturaRetorno.PossuiErro)
                    ViewBag.Erro = estruturaRetorno.Mensagem;
                else
                    ViewBag.Sucesso = estruturaRetorno.Mensagem;

                return View(fornecedor);
            }

            var listaEstados = EstadoSvc.ListaEstados();
            var auxFornecedor = FornecedorSvc.GetFornecedorPorId(fornecedor.Id);

            fornecedor.Cidade = auxFornecedor.Cidade;
            fornecedor.Contato = auxFornecedor.Contato;

            ViewBag.Estado = new SelectList(listaEstados, "Id", "Nome", fornecedor.Cidade.Estado.Nome);

            return View(fornecedor);
        }

        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            FornecedorSvc.Delete(Convert.ToInt32(id));

            return RedirectToAction("Index");
        }
    }
}