﻿using GestaoFornecedor.Core;
using GestaoFornecedor.Domain;
using System;
using System.Web.Mvc;

namespace GestaoFornecedor.Web.Controllers
{
    public class ContatoController : Controller
    {
        // GET: Contato
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult CreateContato(Contato contato)
        {
            var service = new ContatoSvc();
            service.AdicionarContato(contato);

            return RedirectToAction("Editar", "Fornecedor", new { id = contato.FornecedorId });
        }

        public ActionResult ListaContatos(int idFornecedor)
        {
            var service = new ContatoSvc();

            return PartialView("_TabelaContato", service.ListaContatoPorFornecedor(idFornecedor));
        }

        public ActionResult EditarContato(int id)
        {
            var service = new ContatoSvc();
            Contato result = new Contato();

            result = service.GetContatoPorId(id);

            return PartialView("_EditarContato", result);
        }

        [HttpPost]
        public ActionResult EditarContato(Contato contato)
        {
            var service = new ContatoSvc();
            service.Editar(contato);

            return RedirectToAction("Editar", "Fornecedor", new { id = contato.FornecedorId });
        }

        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            var service = new ContatoSvc();
            var contato = service.GetContatoPorId(Convert.ToInt32(id));

            service.Delete(Convert.ToInt32(id));

            return RedirectToAction("Editar", "Fornecedor", new { id = contato.FornecedorId });
        }
    }
}